#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:58983758:7b2711ed726a7f3e61a5de989506af8fa71e9527; then
  applypatch --bonus /system/etc/recovery-resource.dat \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:45520202:fe0f5863180ca813a130962f332ee8696ed7ff46 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:58983758:7b2711ed726a7f3e61a5de989506af8fa71e9527 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
